# Automad Template Language 

This extensions adds syntax highlighting and code snippets for the [Automad](https://automad.org) template language.